package main

import (
	"encoding/json"
	"errors"
	"html/template"
	"io"
	"log/slog"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

var (
	Version string
	Commit  string
	Repo    string
)

func serverPort() string {
	port := os.Getenv("SERVER_PORT")
	if port != "" {
		return port
	}
	return "8080"
}

func siteName() string {
	name := os.Getenv("SITE_NAME")
	if name != "" {
		return name
	}
	return "Echo Server"
}

func siteURL(r *http.Request) string {
	u := os.Getenv("SITE_URL")
	if u != "" {
		return u
	} else if r.Header.Get("Host") != "" {
		return r.Header.Get("Host")
	}
	return "localhost"
}

type echoResponse struct {
	UserAgent   string     `json:"userAgent"`
	QueryParams url.Values `json:"queryParams"`
	Method      string     `json:"method"`
	RequestURI  string     `json:"requestUri"`
	Protocol    string     `json:"protocol"`
	GeneratedAt time.Time  `json:"generatedAt"`
	SiteName    string     `json:"-"`
	SiteURL     string     `json:"-"`
	BuildInfo   `json:"buildInfo"`
}

type BuildInfo struct {
	Version string `json:"version"`
	Commit  string `json:"commit"`
	Repo    string `json:"repo"`
}

func newEchoResponse(r *http.Request) echoResponse {
	return echoResponse{
		UserAgent:   r.UserAgent(),
		QueryParams: r.URL.Query(),
		Method:      r.Method,
		RequestURI:  r.RequestURI,
		Protocol:    r.Proto,
		SiteName:    siteName(),
		SiteURL:     siteURL(r),
		GeneratedAt: time.Now(),
		BuildInfo: BuildInfo{
			Version: Version,
			Commit:  Commit,
			Repo:    Repo,
		},
	}
}

// echoHandler writes the response as json
func echoHandler(w http.ResponseWriter, r *http.Request) {
	slog.Info("received request", "requestPath", r.URL.Path, "remoteAddr", r.RemoteAddr)
	res := newEchoResponse(r)
	b, err := json.Marshal(res)
	if err != nil {
		slog.Error("unable to marshal echoResponse", "error", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, err := io.WriteString(w, "Server Error")
		if err != nil {
			slog.Error("something went really wrong", "error", err)
		}
		return
	}
	w.Header().Add("content-type", "application/json")
	_, err = w.Write(b)
	if err != nil {
		slog.Error("unable to write json response", "error", err)
		return
	}
}

// htmlHandler writes html content to the response based on a template
func htmlHandler(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles("./templates/echo.html")
	if err != nil {
		slog.Error("error parsing the template", "error", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	res := newEchoResponse(r)
	err = tmpl.Execute(w, res)
	if err != nil {
		slog.Error("error writing data to the template", "error", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

// contentHandler determines the correct handler to pass the request to
func contentHandler(w http.ResponseWriter, r *http.Request) {
	slog.Info("received a message", "remoteAddr", r.RemoteAddr,
		"method", r.Method,
		"requestUrlPath", r.URL.Path,
		"headers", r.Header,
		"userAgent", r.UserAgent())
	// force discord and slack to html
	if isSocialBot(r.UserAgent()) {
		htmlHandler(w, r)
	} else if strings.Contains(r.Header.Get("Accept"), "text/html") {
		htmlHandler(w, r)
	} else {
		echoHandler(w, r)
	}
}

// isSocialBot checks user agent strings for hints that the request comes from social media backends used for unfurling links
func isSocialBot(agent string) bool {
	return strings.Contains(agent, "Discordbot") || strings.Contains(agent, "Slackbot")
}

// health is a healthcheck endpoint
func health(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNoContent)
}

func main() {
	slog.Info("starting server and listening", "port", serverPort())

	mux := http.NewServeMux()
	mux.HandleFunc("/api/", contentHandler)
	mux.HandleFunc("/health", health)

	fs := http.FileServer(http.Dir("./static"))
	mux.Handle("/", fs)
	err := http.ListenAndServe(":"+serverPort(), mux) //nolint
	if errors.Is(err, http.ErrServerClosed) {
		slog.Error("server closed")
	} else if err != nil {
		slog.Error("unexpected error", "error", err)
	}
}
