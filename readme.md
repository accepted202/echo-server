# Echo Server
A simple server for testing your http clients.

## Context
This echo server is part of a larger hobby project demonstrating modern ci/cd practices.

Behind the scenes this project deploys to [Kubernetes](https://kubernetes.io) (specifically [Microk8s](https://microk8s.io)). The k8s manifests can be viewed in the [deploy](./deploy) directory.

The continuous integration and continuous delivery aspects of this project are provided by [Gitlab CI](https://docs.gitlab.com/ee/ci/) via the [to be continuous](https://to-be-continuous.gitlab.io/doc/) project (tbc). The configuration can be viewed in the [.gitlab-ci.yml](./.gitlab-ci.yml) file. The starting components used from tbc are their [golang](https://gitlab.com/to-be-continuous/golang), [docker](https://gitlab.com/to-be-continuous/docker), and [semantic-release](https://gitlab.com/to-be-continuous/semantic-release) projects.

Continuous deployment is provided by [Argo CD](https://argoproj.github.io/cd/).

Continuous security scanning is provided by the [trivy-operator](https://github.com/aquasecurity/trivy-operator).

## Features

- Content Negotiation
  - When text/html is included in the "Accept" header, will respond with html content
  - All other Accept types result in a json response
- Open Graph tags. Can help you better understand social media integrations. E.g. type an echo server url into slack and watch the link unfurl right there with call info
- Response includes the following information
  - Your raw user agent string
  - The query parameters you provided
  - The request method
  - The full request URI
  - The protocol version of the request
  - The time the response was generated
- Configurable for your needs via environment variables
  - Set the port you need
  - Set the name of the site
  - Set the base url of the site
 
## Usage

1. Start the server 
2. For the echo response use /api/* (e.g. /api/foo/bar?baz=qux to see a response