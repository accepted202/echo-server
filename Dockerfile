FROM golang:bookworm AS build
ENV GOOS=linux
ENV GOARCH=amd64
ARG VERSION=development
ARG VCS_COMMIT=aaa
ARG REPO_URL=git
ARG BUILD_TIME
WORKDIR /app
COPY go.mod ./
RUN go mod download
COPY *.go ./
RUN CGO_ENABLED=0 GOOS=${GOOS} GOARCH=${GOARCH} go build \
    -ldflags="-s -w -X main.Version=${VERSION} -X main.Commit=${VCS_COMMIT} -X main.Repo=${REPO_URL}" \
    -o /echo-server

FROM alpine:3
WORKDIR /app
COPY static/ ./static/
COPY templates/ ./templates/
COPY --from=build /echo-server ./echo-server
CMD ["./echo-server"]
